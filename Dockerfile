# This is a Dockerfile for building and running a Python 3.9 application in Alpine Linux.
#
# The build stage installs necessary build tools and dependencies,
# then copies the application code and runs unit and integration tests.
#
# The production stage installs only necessary runtime dependencies,
# then copies only the application code from the build stage and sets a non-root user for security.
#
# Exposes port 8000 for communication with other services.

# Build stage
FROM python:3.9-alpine AS build

# copy files required for installation of dependencies
COPY requirements-build.txt setup.py .

# install necessary build dependencies and tools, and run pip
# to install packages required only for building project dependencies and dev environment,
# then delete unnecessary packages installed for debugging purpose
RUN apk add --no-cache gcc musl-dev libffi-dev openssl-dev && \
    pip install --no-cache-dir -r requirements-build.txt && \
    apk del gcc musl-dev libffi-dev openssl-dev

# copy all the source files and test files to the container
COPY . /app

# set working directory
WORKDIR /app

# run all unit and integration tests
RUN python -m unittest discover -s tests

# build wheel
RUN python -m build

# Production stage
FROM python:3.9-alpine

# set working directory
WORKDIR /app

# copy files required for installation of runtime packages
COPY requirements-run.txt .

# install runtime packages required for running the application
RUN pip install --no-cache-dir -r requirements-run.txt

# copy only the producer.py script from the build stage
COPY --from=build /app/dist/ /app

RUN pip install *.whl

# create a new non-root user named appuser with no password
RUN adduser --disabled-password appuser && \
    # set ownership of the /app directory to the appuser user
    chown -R appuser /app

# switch to the appuser user for better security
USER appuser

# expose port 8000 for communicating with other services
EXPOSE 8000

ENV PYTHONUNBUFFERED=1

# start the application by running the producer.py script
CMD ["python", "/usr/local/lib/python3.9/site-packages/producer.py"]
